/*
* Copyright 2022 TrustiPhi, LLC.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
* 	http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

syntax = "proto3";
package triathlon;

option optimize_for = LITE_RUNTIME;

message UInt32Value {
  uint32 value = 1;
}

message StringValue {
  string value = 1;
}

message Any {
  string type_url = 1;
  bytes value = 2;
}

message TriathlonMsg {
  uint32 msg_version = 1;
  Any msg_content = 2;
}

enum ServiceType {
  DEVICE_MGMT_SERVICE = 0;
  DEVICE_CXN_SERVICE = 1;
  AUTHENTA_NAXOS_CONFIG_SERVICE = 2;
  CLIENT_DB_SERVICE = 3;
  DEVICE_AGENT = 4;
  DEVICE_EVENT = 5;
  FILE_SERVICE = 6;
  MICRON_KMS_INTERFACE_SERVICE = 7;
  SYSTEM_OPERATIONS_CREDENTIAL_AUTHORITY_SERVICE = 8;
  AUTHENTA_KMP_INTERFACE_SERVICE = 9;
  KEY_IMPORT_SERVICE = 10;
  CLIENT_API_GATEWAY = 11;
  AUTHENTA_PLATFORM_IDENTITY_OPERATIONS_SERVICE = 12;
  EXTERNAL_API_FEATURE_SERVICE = 13;
  AUTHENTA_INDIGO_CONFIG_SERVICE = 14;
}

message MsgContext {
  uint32 cxn_hndl = 1;
  ServiceType sender = 2;
  ServiceType receiver = 3;
  string reply_to = 4;
  string correlation_id = 5;
  Any sub_context = 6;
}

message MsgContent {
  ServiceType msg_type = 1;
  Any msg_data = 2;
  MsgContext msg_context = 3;
}

// DeviceAgentMsg
enum DeviceAgentMsgType {
  DEVICE_AGENT_CORE_OPS = 0;
  SEC_MOD_MGR = 1;
  SEC_MOD_NATIVE = 2;
}

message DeviceAgentMsg {
  DeviceAgentMsgType msg_type = 1;
  Any msg_data = 2;
}

// DeviceAgentCoreOpsMsg
enum DeviceAgentCoreOpsMsgType {
  DEVICE_INFO_PACKAGE_REQUEST = 0;
  DEVICE_INFO_PACKAGE_RESPONSE = 1;
  GET_REG_TOKEN_REQUEST = 2;
  GET_REG_TOKEN_RESPONSE = 3;
  SET_REG_DEVICE_ID_REQUEST = 4;
  SET_REG_DEVICE_ID_RESPONSE = 5;
  DISCONNECT_DEVICE_NOTIFICATION = 6;
}

message DeviceAgentCoreOpsMsg {
  DeviceAgentCoreOpsMsgType msg_type = 1;
  Any msg_data = 2;
}

message DeviceInfoPackageRequest {}

enum SecModStatus {
  READY = 0;
  NOT_AVAILABLE = 1;
}

message SecModInfo {
  string sec_mod_logical_name = 1;
  SecModType sec_mod_type = 2;
  uint32 sec_mod_hndl = 3;
  SecModStatus sec_mod_status = 4;
  uint32 flash_erase_size = 5;
}

enum DevicePlatformIdentifier {
  UNKNOWN = 0;
}

enum DevicePersistentStorageCapabilities {
  READ_WRITE = 0;
  READ_ONLY = 1;
}

message DeviceInfoPackageResponse {
  UInt32Value device_id = 1;
  uint32 num_sec_mods = 2;
  repeated SecModInfo sec_mod_info = 3;
  bool dynamic_reg_unsupported = 4;
  bytes device_gen_device_id = 5;
  uint32 device_agent_major_version_num = 6;
  uint32 device_agent_minor_version_num = 7;
  DevicePlatformIdentifier device_platform_identifier = 8;
  DevicePersistentStorageCapabilities device_persistent_storage_caps = 9;
}

message GetRegTokenRequest {}

message GetRegTokenResponse {
  StringValue token = 1;
}

message SetRegDeviceIdRequest {
  UInt32Value device_id = 1;
}

message SetRegDeviceIdResponse {
  uint32 return_code = 1;
}

message DisconnectDeviceNotification {}

// SecModMgrMsg
message SecModMgrMsg {
  uint32 sec_mod_hndl = 1;
  SecModMgrMsgData msg_data = 2;
  uint32 partition_id = 3;
}

message SecModMgrMsgData {
  SecModMgrMsgType msg_type = 1;
  Any msg_data = 2;
}

enum SecModMgrErrCode {
  SMM_ERR_OK = 0;
  SMM_ERR_UNKNOWN = 1;
  SMM_ERR_COMM = 2;         // network interruption
}

enum SecModMgrMsgType {
  FILE_DOWNLOAD_QUERY_STATUS_REQUEST = 0;
  FILE_DOWNLOAD_QUERY_STATUS_RESPONSE = 1;
  FILE_DOWNLOAD_PREPARE_REQUEST = 2;
  FILE_DOWNLOAD_PREPARE_RESPONSE = 3;
  FILE_DOWNLOAD_START_REQUEST = 4;
  FILE_DOWNLOAD_START_RESPONSE = 5;
  FILE_DOWNLOAD_RESUME_REQUEST = 6;
  FILE_DOWNLOAD_RESUME_RESPONSE = 7;
  FILE_DOWNLOAD_COMPLETE_NOTIF = 8;
  WRITE_FILE_TO_FLASH_PREPARE_REQUEST = 9;
  WRITE_FILE_TO_FLASH_PREPARE_RESPONSE = 10;
  WRITE_FILE_TO_FLASH_ASYNC_WRITE_START_REQUEST = 11;
  WRITE_FILE_TO_FLASH_ASYNC_WRITE_START_RESPONSE = 12;
  WRITE_FILE_TO_FLASH_COMPLETE_NOTIF = 13;
  WRITE_FILE_TO_FLASH_DONE_REQUEST = 14;
  WRITE_FILE_TO_FLASH_DONE_RESPONSE = 15;
  LEGACY_BLOCK_QUERY_STATUS_REQUEST = 16;
  LEGACY_BLOCK_QUERY_STATUS_RESPONSE = 17;
  LEGACY_BLOCK_MODIFY_LOCK_REQUEST = 18;
  LEGACY_BLOCK_MODIFY_LOCK_RESPONSE = 19;
  LEGACY_BLOCK_ERASE_REQUEST = 20;
  LEGACY_BLOCK_ERASE_RESPONSE = 21;
  LEGACY_BLOCK_READ_REQUEST = 22;
  LEGACY_BLOCK_READ_RESPONSE = 23;
  FILE_DOWNLOAD_PROGRESS_NOTIF = 24;
}

enum FileDownloadStatus {
  FD_NOT_STARTED = 0;
  FD_IN_PROGRESS = 1;
  FD_COMPLETED = 2;
}

message FileDownloadQueryStatusRequest {
  string file_id = 1;
  uint64 file_size = 2;
  uint32 device_id = 3;
}

message FileDownloadQueryStatusResponse {
  bool success = 1;
  uint32 device_id = 2;
  string file_id = 3;
  FileDownloadStatus status = 4;  
  bool has_cert = 5;  
}

message FileDownloadPrepareRequest {
  string file_id = 1;
  bool resume = 2;
}

message FileDownloadPrepareResponse {
  bool success = 1;
  uint32 device_id = 2;
  string file_id = 3;
  string csr = 4;
  bool resume = 5;
}

message FileDownloadStartRequest {
  string file_id = 1;
  string file_uri = 2;
  string client_cert = 3;
  string server_cert = 4; 
}

message FileDownloadStartResponse {
  bool success = 1;
  string file_id = 2;
}

message FileDownloadResumeRequest {
  string file_id = 1;
  string file_uri = 2;
}

message FileDownloadResumeResponse {
  bool success = 1; 
  string file_id = 2; 
}

message FileDownloadProgressNotif {
  string file_id = 1;
  uint64 offset = 2;
  uint64 size_downloaded = 3;
}

message FileDownloadCompleteNotif {
  uint32 device_id = 1;
  string file_id = 2;
  SecModMgrErrCode err_code = 3;
}

message WriteFileToFlashPrepareRequest {
  string file_id = 1;
  uint64 file_size = 2;
  bytes decrypt_key = 3;
  bool resume = 4;
  bool query_only = 5;
}

message WriteFileToFlashPrepareResponse {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  string file_id = 3;
  uint32 est_write_performance = 4;
  uint64 num_bytes_written = 5;
}

message WriteFileToFlashAsyncWriteStartRequest {
  string file_id = 1;
  uint64 file_offset = 2;
  uint64 dest_addr = 3;
  uint32 num_bytes = 4;
}

message WriteFileToFlashAsyncWriteStartResponse {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  string file_id = 3;
  uint64 file_offset = 4;
  uint64 dest_addr = 5;
  uint32 num_bytes = 6;
}

message WriteFileToFlashCompleteNotif {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  string file_id = 3;
  uint64 file_offset = 4;
  uint64 dest_addr = 5;
  uint32 num_bytes = 6;
  uint32 num_bytes_written = 7;
  bytes digest_bytes_written = 8;
}

message WriteFileToFlashDoneRequest {
  string file_id = 1;
  bool delete_file = 2;
}

message WriteFileToFlashDoneResponse {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  uint32 device_id = 3;
  string file_id = 4;
}

enum LegacyLockType {
  LOCK_VOLATILE = 0;
  LOCK_PERSISTENT = 1;
}

message LegacyBlockQueryStatusRequest {
  bool is_upper_die = 1;
}

message LegacyBlockQueryStatusResponse {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  bytes volatile_lock_states = 3;
  bytes persistent_lock_states = 4;
}

message LegacyBlockModifyLockRequest {
  LegacyLockType lock_type = 1;
  bytes block_indices = 2;
  bool lock = 3;
  bool is_upper_die = 4;
}

message LegacyBlockModifyLockResponse {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  LegacyLockType lock_type = 3;
  bytes block_indices = 4;
  bool lock = 5;
}

message LegacyBlockEraseRequest {
  uint64 addr = 1;
  uint64 size = 2;
}

message LegacyBlockEraseResponse {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  uint64 addr = 3;
  uint64 size = 4;
}

message LegacyBlockReadRequest {
  uint64 addr = 1;
  uint32 size = 2;
}

message LegacyBlockReadResponse {
  SecModMgrErrCode err_code = 1;
  string err_description = 2;
  bytes data = 3;
  uint64 addr = 4;
  uint32 size = 5;
}

// SecModNativeMsg
enum SecModType {
  AUTHENTA_NAXOS = 0;
  AUTHENTA_INDIGO = 1;
  TPM = 2;
}

message SecModNativeMsgData {
  SecModType sec_mod_type = 1;
  Any msg_data = 2;
}

message SecModNativeMsg {
  uint32 sec_mod_hndl = 1;
  SecModNativeMsgData msg_data = 2;
  uint32 partition_id = 3;
}

enum AuthentaNaxosCmdType {
  AUTHENTA_NAXOS_ACQUIRE_REQUEST = 0;
  AUTHENTA_NAXOS_ACQUIRE_RESPONSE = 1;
  AUTHENTA_NAXOS_RELEASE_REQUEST = 2;
  AUTHENTA_NAXOS_RELEASE_RESPONSE = 3;
  AUTHENTA_NAXOS_READ_GPRR_REQUEST = 4;
  AUTHENTA_NAXOS_READ_GPRR_RESPONSE = 5;
  AUTHENTA_NAXOS_CMD_REQUEST = 6;
  AUTHENTA_NAXOS_CMD_RESPONSE = 7;
  AUTHENTA_NAXOS_READ_STATUS_REGISTER_REQUEST = 8;
  AUTHENTA_NAXOS_READ_STATUS_REGISTER_RESPONSE = 9;
}

message AuthentaNaxosMsgData {
  AuthentaNaxosCmdType cmd_type = 1;
  Any msg_data = 2;
}

message AuthentaNaxosAcquireRequest {}

message AuthentaNaxosAcquiureResponse {
  uint32 return_code = 1;
}

message AuthentaNaxosReleaseRequest {}

message AuthentaNaxosReleaseResponse {
  uint32 return_code = 1;
}

message AuthentaNaxosCmdRequest {
  bytes cmd = 1;
}

message AuthentaNaxosCmdResponse {
  uint32 return_code = 1;
}

message AuthentaNaxosReadGPRRRequest {}

message AuthentaNaxosReadGPRRResponse {
  bytes response = 1;
}

message AuthentaNaxosReadStatusRegisterRequest {}

message AuthentaNaxosReadStatusRegisterResponse {
  uint32 return_code = 1;
}

enum AuthentaIndigoCmdType {
  AUTHENTA_INDIGO_ACQUIRE_REQUEST = 0;
  AUTHENTA_INDIGO_ACQUIRE_RESPONSE = 1;
  AUTHENTA_INDIGO_RELEASE_REQUEST = 2;
  AUTHENTA_INDIGO_RELEASE_RESPONSE = 3;
  AUTHENTA_INDIGO_CMD_REQUEST = 4;
  AUTHENTA_INDIGO_CMD_RESPONSE = 5;
}

message AuthentaIndigoMsgData {
  AuthentaIndigoCmdType cmd_type = 1;
  Any msg_data = 2;
}

message AuthentaIndigoAcquireRequest {}

message AuthentaIndigoAcquiureResponse {
  uint32 return_code = 1;
}

message AuthentaIndigoReleaseRequest {}

message AuthentaIndigoReleaseResponse {
  uint32 return_code = 1;
}

message AuthentaIndigoCmdRequest {
  bytes cmd = 1;
  uint32 rsp_size = 2;
}

message AuthentaIndigoCmdResponse {
  uint32 return_code = 1;
  bytes rsp = 2;
}
